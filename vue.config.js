module.exports = {
  publicPath: process.env.NODE_ENV === 'production'? '': '/',
  
  devServer: {
    proxy: {
      '/api': {
        target: 'http://218.205.68.77:8080/zjweb/', //要做代理的接口
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  }
}