import Vue from 'vue'
import Router from 'vue-router'
import PageHome from '@/pages/PageHome'
import PageThreadShow from '@/pages/PageThreadShow'
import PageNotFound from '@/pages/PageNotFound'
import PageForum from '@/pages/PageForum'
import PageCategory from '@/pages/PageCategory'
import Profile from '@/pages/PageProfile'
import ThreadCreate from '@/pages/PageThreadCreate'
import PageThreadEdit from '@/pages/PageThreadEditor'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: PageHome
    },
    {
      path: '/category/:id',
      name: 'Category',
      component: PageCategory,
      props: true
    },
    //注意ThreadCreate和ThreadShow顺序！！！！
    {
      path: '/thread/create/:forumId',
      name: 'ThreadCreate',
      component: ThreadCreate,
      props: true
    },
    {
      path: '/thread/:id',// 若非history模式，指的是锚标签后的参数 eg #/thread/xxxxx
      name: 'ThreadShow',
      component: PageThreadShow,
      props: true,// 在组件中使用 $route 会使之与其对应路由形成高度耦合，从而使组件只能在某些特定的 URL 上使用，限制了其灵活性。使用 props 将组件和路由解耦
    },
    {
      path: '/thread/:id/edit',// 若非history模式，指的是锚标签后的参数 eg #/thread/xxxxx
      name: 'ThreadEdit',
      component: PageThreadEdit,
      props: true,// 在组件中使用 $route 会使之与其对应路由形成高度耦合，从而使组件只能在某些特定的 URL 上使用，限制了其灵活性。使用 props 将组件和路由解耦
    },        
    {
      path: '/forum/:id',
      name: 'Forum',
      component: PageForum,
      props: true // 如果 props 被设置为 true，route.params 将会被设置为组件属性。(布尔模式)
    },
    {
      path: '/me',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/me/edit',
      name: 'ProfileEdit',
      component: Profile,
      props: {
        edit: true //对象模式 如果 props 是一个对象，它会被按原样设置为组件属性。当 props 是静态的时候有用。
      }
    },
    {
      path: '*',
      name: 'NotFound',
      component: PageNotFound
    }
  ],
  // mode: 'history'
})

/*
  '/thread'与'/thread/xxxxx'不是一个路由 
  '/thread/:id'才是动态路由

*/