import Vue from 'vue'
import Vuex from 'vuex'
import sourceData from '@/data'
import {countObjectProperties} from '@/utils'
Vue.use(Vuex)

const makeAppendChildToParentMutation = ({parent, child}) =>
  (state, {childId, parentId}) => {
    const resource = state[parent][parentId]
    if (!resource[child]) {
      Vue.set(resource, child, {})
    }
    Vue.set(resource[child], childId, childId)
  }





export default new Vuex.Store({
  state: {
    ...sourceData,
    authId: 'VXjpr2WHa8Ux4Bnggym8QFLdv5C3'
  },
  getters: {
    authUser(state) {
      return state.users[state.authId]
    },
    userPostsCount: state => id => countObjectProperties(state.users[id].posts),
    userThreadsCount: state => id => countObjectProperties(state.users[id].threads),
    threadRepliesCount: state => id => countObjectProperties(state.threads[id].posts) - 1
  },
  actions: {
    createPost ({commit, state}, post) {
      const postId = 'greatPost' + Math.random()
      post['.key'] = postId
      post.userId = state.authId
      post.publishedAt = Math.floor(Date.now() / 1000)
      commit('setPost', {post, postId})
      // commit('appendPostToThread', {postId, threadId: post.threadId})
      // commit('appendPostToUser', {postId, userId: post.userId})
      commit('appendPostToThread', {parentId: post.threadId, childId: postId})
      commit('appendPostToUser', {parentId: post.userId, childId: postId})
    },
    updatePost ({commit, state}, {id, text}) {
      return new Promise((resolve, reject) => {
        const post = state.posts[id]
        commit('setPost', {
          postId: id,
          post: {
            ...post,
            text,
            edited: {
              at: Math.floor(Date.now() / 1000),
              by: state.authId
            }
          }
        })
        resolve(post)
      })
    },
    updateUser ({commit}, user) {
      commit('setUser', {user})
    },
    createThread ({commit, state, dispatch}, {title, text, forumId}) {
      return new Promise((resolve, reject) => {
        const threadId = 'greatThread' + Math.random()
        const userId = state.authId
        const publishedAt = Math.floor(Date.now() / 1000)
        const thread = {forumId, title, userId, publishedAt, '.key': threadId}
        commit('setThread', {thread, threadId})
        // commit('appendThreadToForum', {forumId, threadId})
        // commit('appendThreadToUser', {userId, threadId})
        commit('appendThreadToForum', {parentId: forumId, childId: threadId})
        commit('appendThreadToUser', {parentId: userId, childId: threadId})
        const post = {text, threadId}
        dispatch('createPost', post)
        resolve(state.threads[threadId])
      })
    },
    updateThread ({commit, state, dispatch}, {title, text, id}) {
      return new Promise((resolve, reject) => {
        const thread = state.threads[id]
        // const post = state.posts[thread.firstPostId]
        // thread.title = title // 不能这样写！！！！ 
        //因为这样会直接改变state内的数据。这与vuex的数据管理模式不符合
        const newThread = {...thread, title}
        commit('setThread', {thread: newThread, threadId: id})
        // commit('setPost', {postId: newPost['.key'], post: newPost})
        dispatch('updatePost', {id: thread.firstPostId, text})
          .then(() => resolve(newThread))
      })
    }
  },
  mutations: { //第一个参数是state, 第二个参数是payload（载荷）
    setPost (state, {postId, post}) {
      Vue.set(state.posts, postId, post);
    },
    appendPostToThread: makeAppendChildToParentMutation({parent: 'threads', child: 'posts'}),
    appendThreadToForum: makeAppendChildToParentMutation({parent: 'forums', child: 'threads'}),
    appendPostToUser: makeAppendChildToParentMutation({parent: 'users', child: 'posts'}),
    appendThreadToUser: makeAppendChildToParentMutation({parent: 'users', child: 'threads'}),
    // appendPostToThread (state, {postId, threadId}) {
    //   const thread = state.threads[threadId]
    //   if (!thread.posts) { // 刚添加的
    //     Vue.set(thread, 'posts', {})
    //   }
    //   Vue.set(thread.posts, postId, postId)
      
    // },
    
    // appendThreadToForum (state, {forumId, threadId}) {
    //   const forum = state.forums[forumId]
    //   if (!forum.threads) {
    //     Vue.set(forum, 'threads', {})
    //   } 
    //   Vue.set(forum.threads, threadId, threadId)
      
    // },

    // appendThreadToUser (state, {userId, threadId}) {
    //   const user = state.users[userId]
    //   if (!user.threads) {
    //     Vue.set(user, 'threads', {})
    //   }
    //   Vue.set(user.threads, threadId, threadId)
    // },
    
    // appendPostToUser (state, {postId, userId}) {
    //   const user = state.users[userId]
    //   Vue.set(user.posts, postId, postId)
    // },
    setUser (state, {user}) {
      const userId = state.authId
      Vue.set(state.users, userId, user)
    },
    setThread (state, {thread, threadId}) {
      Vue.set(state.threads, threadId, thread)
    }
    
  }
})