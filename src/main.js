import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from '@/store/index'
import AppDate from '@/components/AppDate'
Vue.component('AppDate', AppDate) //注册或获取全局组件。注册还会自动使用给定的id设置组件的名称
  //Vue.component( id, [definition] )
Vue.config.productionTip = false
import showAlert from '@/dialog/dialog'
Vue.prototype.$showAlert = showAlert

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
