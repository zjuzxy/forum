import Vue from 'vue'
import dialog from './dialog.vue' //引入dialog只是用其dom结构
// 第一步： 创建实例 第二步： 挂载到元素上
let MyAlertConstructor = Vue.extend(dialog)
let instance
const showAlert = function(remindMsg) {
  instance = new MyAlertConstructor({
    data: {
      remindMsg: remindMsg,
      visible: true
    },
    methods: {}
  })
  instance.$mount()
  document.body.appendChild(instance.$el)
  return instance
} 
export default showAlert