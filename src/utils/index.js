/*
 * @Author: zjuzxy 
 * @Date: 2019-03-27 19:57:57 
 * @Last Modified by: zjuzxy
 * @Last Modified time: 2019-03-27 19:58:43
 */
const countObjectProperties = obj => {
  if (typeof obj === 'object') {
    return Object.keys(obj).length
  }
  return 0
}
/**
 * 数组去重
 * @param {Array} items 
 */
const getUniqueItems = items => items.filter((item, index) => index === items.indexOf(item))


export {
  countObjectProperties,
  getUniqueItems
}
