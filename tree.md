.
├── README.md
├── babel.config.js
├── dist
│   ├── css
│   │   ├── app.6e35c498\ 3.css
│   │   └── app.6e35c498.css
│   ├── favicon.ico
│   ├── index.html
│   └── js
│       ├── app.ed7b53f0.js
│       ├── app.ed7b53f0.js.map
│       ├── chunk-vendors.b183a22c\ 3.js
│       ├── chunk-vendors.b183a22c.js
│       ├── chunk-vendors.b183a22c.js\ 3.map
│       └── chunk-vendors.b183a22c.js.map
├── package-lock.json
├── package.json
├── postcss.config.js
├── public
│   ├── css
│   ├── favicon.ico
│   └── index.html
├── src
│   ├── App.vue
│   ├── assets
│   │   ├── css
│   │   └── logo.png
│   ├── components
│   │   ├── AppDate.vue
│   │   ├── ForumList.vue
│   │   ├── ForumListItem.vue
│   │   ├── PostEditor.vue
│   │   ├── PostList.vue
│   │   ├── PostListItem.vue
│   │   ├── ThreadList.vue
│   │   └── ThreadListItem.vue
│   ├── data.json
│   ├── main.js
│   ├── pages
│   │   ├── PageForum.vue
│   │   ├── PageHome.vue
│   │   ├── PageNotFound.vue
│   │   └── PageThreadShow.vue
│   ├── router.js
│   ├── store.js
│   └── views
├── tree.md
└── vue.config.js

11 directories, 37 files
